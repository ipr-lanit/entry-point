package ru.lanit.ipr.ipushkarev.entrypoint.cdi;

import ru.lanit.ipr.ipushkarev.entrypoint.model.dto.request.UserRegistrationRequestDto;
import ru.lanit.ipr.ipushkarev.entrypoint.model.dto.response.UserRegistrationResponseDto;
import ru.lanit.ipr.ipushkarev.entrypoint.service.api.UserService;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
@SessionScoped
public class UserBean implements Serializable {

    private static final long serialVersionUID = 2735877061888781517L;

    @Inject
    private UserService userService;

    private String username;

    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void registration() {
        UserRegistrationRequestDto registrationRequest = new UserRegistrationRequestDto(getUsername(), getPassword());
        UserRegistrationResponseDto registrationResponse = userService.registration(registrationRequest);

        if (registrationResponse.isResult()) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.html");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Contact admin.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
}
