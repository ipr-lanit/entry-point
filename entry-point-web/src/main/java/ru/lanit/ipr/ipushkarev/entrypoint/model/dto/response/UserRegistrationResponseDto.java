package ru.lanit.ipr.ipushkarev.entrypoint.model.dto.response;

import java.util.List;

public class UserRegistrationResponseDto {

    private String username;

    private String password;

    private boolean result;

    private List<String> errors;

    public UserRegistrationResponseDto(String username, String password, boolean result, List<String> errors) {
        this.username = username;
        this.password = password;
        this.result = result;
        this.errors = errors;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
