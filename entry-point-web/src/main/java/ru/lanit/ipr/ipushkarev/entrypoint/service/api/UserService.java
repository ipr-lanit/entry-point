package ru.lanit.ipr.ipushkarev.entrypoint.service.api;

import ru.lanit.ipr.ipushkarev.entrypoint.model.dto.request.UserRegistrationRequestDto;
import ru.lanit.ipr.ipushkarev.entrypoint.model.dto.response.UserRegistrationResponseDto;

import javax.ejb.Local;

@Local
public interface UserService {

    UserRegistrationResponseDto registration(UserRegistrationRequestDto registrationRequest);
}
