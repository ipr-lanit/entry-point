package ru.lanit.ipr.ipushkarev.entrypoint.service.impl;

import ru.lanit.ipr.ipushkarev.entrypoint.model.dto.request.UserRegistrationRequestDto;
import ru.lanit.ipr.ipushkarev.entrypoint.model.dto.response.UserRegistrationResponseDto;
import ru.lanit.ipr.ipushkarev.entrypoint.service.api.UserService;
import ru.lanit.ipr.ipushkarev.userservice.ws.api.CreateUserRequestDto;
import ru.lanit.ipr.ipushkarev.userservice.ws.api.CreateUserResponseDto;
import ru.lanit.ipr.ipushkarev.userservice.ws.impl.UserWebServiceImplService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;

@Stateless
public class UserServiceImpl implements UserService, Serializable {

    private static final long serialVersionUID = 4935791720239457333L;

    @Inject
    private UserWebServiceImplService userWebServiceImpl;

    @Override
    public UserRegistrationResponseDto registration(UserRegistrationRequestDto registrationRequest) {
        CreateUserRequestDto createUserRequestDto = new CreateUserRequestDto();
        createUserRequestDto.setUsername(registrationRequest.getUsername());
        createUserRequestDto.setPassword(registrationRequest.getPassword());

        CreateUserResponseDto createUserResponseDto = userWebServiceImpl.getUserWebServiceImplPort().createUser(createUserRequestDto);

        return new UserRegistrationResponseDto(
                createUserResponseDto.getUsername(),
                createUserResponseDto.getPassword(),
                createUserResponseDto.isResult(),
                createUserResponseDto.getErrors()
        );
    }
}
